import requests
import itertools

TOKEN = """AKfycbxice34Q0pEmsdWNWCkdla7W8JyCHuDc5stk0ly3i4BRvg4KRWQyJ4V"""
url = "https://script.google.com/macros/s/{token}/exec?service=calculatePrice&email=e.baklanov@innopolis.ru&type={type}&plan={plan}&distance={distance}&planned_distance={planned_distance}&time={time}&planned_time={time}&inno_discount={inno_discount}"

# Here is InnoCar Specs:
# Budet car price per minute = 16
# Luxury car price per minute = 38
# Fixed price per km = 11
# Allowed deviations in % = 15
# Inno discount in % = 6

budget_price_per_min = 16
luxury_price_per_min = 38
fixed_price_per_km = 11
allowed_deviations = 15

BVA = {
    "type": ["budget", "luxury", "nonsense"],
    "plan": ["minute", "fixed_price", "nonsense"],
    "distance": [-10, 0, 1, 1000000],
    "planned_distance": [-10, 0, 1, 1000000],
    "time": [-10, 0, 1, 1000000],
    "planed_time": [-10, 0, 1, 1000000],
    "inno_discount": ["yes", "no"],
}

for list in itertools.product(*BVA.values()):
    print(list)
    # print(
    #     requests.get(
    #         url=url.format(
    #             token=TOKEN,
    #             type=list[0],
    #             plan=list[1],
    #             distance=list[2],
    #             planned_distance=list[3],
    #             time=list[4],
    #             planed_time=list[5],
    #             inno_discount=list[6],
    #         )
    #     ).content
    # )
