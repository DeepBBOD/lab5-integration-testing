# Lab5 -- Integration testing

---

## HOMEWORK SOLUTIONS

### Here are InnoCar Specs:
```
Budet car price per minute = 16
Luxury car price per minute = 38
Fixed price per km = 11
Allowed deviations in % = 15
Inno discount in % = 6
```

### BVA

```json
{
    "type": ["budget", "luxury", "nonsense"],
    "plan": ["minute", "fixed_price", "nonsense"],
    "distance": [-10, 0, 10, 1000000, "nonsense"],
    "planned_distance": [-10, 0, 10, 1000000, "nonsense"],
    "time": [-10, 0, 10, 1000000, "nonsense"],
    "planed_time": [-10, 0, 10, 1000000, "nonsense"],
    "inno_discount": ["yes", "no"]
}
```

| Type | Plan | Distance | Planed distance | Time | Planed time | Discount | Expected result | Result | Bug |
|------|------|----------|-----------------|------|-------------|----------|-----------------|--------|-----|
| "nonsense" | * | * | * | * | * | * | Invalid Request | Invalid Request | no |
| * | "nonsense" | * | * | * | * | * | Invalid Request | Invalid Request | no |
| * | * | -10 | * | * | * | * | Invalid Request | Invalid Request | no |
| * | * | * | -10 | * | * | * | Invalid Request | Invalid Request | no |
| * | * | * | * | -10 | * | * | Invalid Request | Invalid Request | no |
| * | * | * | * | * | -10 | * | Invalid Request | Invalid Request | no |
| * | * | "nonsense" | 10 | 10 | 10 | * | Invalid Request | {"price":null} | yes |
| * | * | 10 | "nonsense" | 10 | 10 | * | Invalid Request | {"price":null} | yes |
| * | * | 10 | 10 | "nonsense" | 10 | * | Invalid Request | {"price":null} | yes |
| * | * | 10 | 10 | 10 | "nonsense" | * | Invalid Request | {"price":null} | yes |
| "luxury" | "fixed_price" | * | * | * | * | * | Invalid Request | Invalid Request | no |
| "budget" | "fixed_price" | 0 | 0 | 10 | 10 | * | {"price":0} (?) | {"price":166.6666} | yes? |
| "budget" | "fixed_price" | 10 | 0 | 10 | * | * | {"price":160} | {"price":166.6666} | yes |
| "budget" | "fixed_price" | 10 | 10 | 10 | 10 | * | {"price":110} | {"price":125} | yes |
| "budget" | "fixed_price" | 1000000 | 1000000 | 0 | 0 | * | {"price":11000000} | {"price":0} | yes |
| "budget" | "minute" | * | * | 0 | * | * | {"price":0} | {"price":0} | no |
| "budget" | "minute" | * | * | 10 | * | * | {"price":160} | {"price":160} | no |
| "budget" | "minute" | * | * | 1000000 | * | * | {"price":16000000} | {"price":16000000} | no |
| "luxury" | "minute" | * | * | 0 | * | * | {"price":0} | {"price":0} | no |
| "luxury" | "minute" | * | * | 10 | * | * | {"price":380} | {"price":266} | yes |
| "luxury" | "minute" | * | * | 1000000 | * | * | {"price":38000000} | {"price":26600000} | yes |


### Decision Table

| Conditions(inputs) | Values | R1 | R2 | R3 | R4 | R5 | R6 | R7 | R8 | R9 |
|--------------------|:-----------------------------:|:----------:|:----------:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| Type               | budget, luxury, nonsense      |  nonsense  | *          |  *  |  *  |  *  |  *  |  luxury  |  budget  |  luxury, budget  |
| Plan               | minute, fixed_price, nonsense |  *         |  nonsense  |  *  |  *  |  *  |  *  |  fixed_price  |  fixed_price  |  minute  |
| Distance           | \>=0, <0, nonsense            |  *         |  *         |  <0, nonsense |  *  |  *  |  *  |  *  |  \>=0  |  \>=0  |
| Planed distance    | \>=0, <0, nonsense            |  *         |  *         |  *  |  <0, nonsense |  *  |  *  |  *  |  \>=0  |  \>=0  |
| Time               | \>=0, <0, nonsense            |  *         |  *         |  *  |  *  |  <0, nonsense |  *  |  *  |  \>=0  |  \>=0  |
| Planed time        | \>=0, <0, nonsense            |  *         |  *         |  *  |  *  |  *  |  <0, nonsense |  *  |  \>=0  |  \>=0  |
| Discount           | yes, no, nonsense             |  *         |  *         |  *  |  *  |  *  |  *  |  *  |  *  |  *  |
|--------------------|-----------|---------|---------|-----|-----|-----|-----|----|----|----|
| Invalid            |                               |  X         |  X         |  X  |  X  |  X  |  X  |  X  |    |    |
| OK                 |                               |            |            |     |     |     |     |     |  X  |  X  |


There are a few bugs I've found:
- Inno Discount does not work at all
- Price is wrong for type `luxury` and plan `minute`. Looks like `Luxury car price per minute` param equals to 26.6
- Also calculations are wrong for type `budget` and plan `fixed_price`
- I'm not sure but probably logic for `Allowed deviations in %` is not working properly. For example if we pass `Distance=1000000` and `Planed distance=1000000` and minutes params set to `0` we get `{"price":0}` as a result. Which is not quite correct I guess. So I think it always calculates price as for `minutes` plan
- For some reason in some cases we get `{"price":null}` when we pass `nonsense` to one of the numeric parameters.

---

## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab5-integration-testing)
2. This time around we have our own variants to test, open [This](https://docs.google.com/spreadsheets/d/1lLigF6um8pKH2IGZNt9xXcl8quPtFPzmVtt5RCLeCqw/edit?usp=sharing) and use your link:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it.
- Now, let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
- We will send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our spec:
        Here is InnoCar Specs:
        Budet car price per minute = 17
        Luxury car price per minute = 33
        Fixed price per km = 11
        Allowed deviations in % = 10
        Inno discount in % = 10
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:
| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_`
Ok, then we can write just one test to test nonsense and few tests for distance <= 0 and 4 tests to check correct work of the system, let's send requests to checck that.




## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

